
include("../lca.jl")

settings = [ Cubes(d1, d2) for d1 in 1:5, d2 in [10] ]
ns = [round(Int, 2^(k/4)) for k in (5*4):(11*4)]
reps = 2000

run = "hoelder-0.5"
r = run_mcsim(settings, ns; reps, cost = Hoelder(0.5), run)
path = joinpath("data", run * ".dat")
save_mcsim(path, r)
println("saved run '$run' under '$path'")

run = "hoelder-1.5"
r = run_mcsim(settings, ns; reps, cost = Hoelder(1.5), run)
path = joinpath("data", run * ".dat")
save_mcsim(path, r)
println("saved run '$run' under '$path'")

