
include("../lca.jl")

settings = [ Shells(d) for d in 1:10 ]
ns = [round(Int, 2^(k/4)) for k in (5*4):(11*4)]
reps = 2000

run = "shells-sqeuclidean"
r = run_mcsim(settings, ns; reps, cost = SqEuclidean(), run)
path = joinpath("data", run * ".dat")
save_mcsim(path, r)
println("saved run '$run' under '$path'")

