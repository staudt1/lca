
include("../lca.jl")

settings = [ Semidiscrete(m, d2) for m in [5, 10, 50], d2 in [10, 100, 1000] ]
ns = [round(Int, 2^(k/4)) for k in (5*4):(15*4)]
reps = 2000

run = "semi-sqeuclidean"
r = run_mcsim(settings, ns; reps, cost = SqEuclidean(), run)
path = joinpath("data", run * ".dat")
save_mcsim(path, r)
println("saved run '$run' under '$path'")

#run = "semi-cityblock"
#r = run_mcsim(settings, ns; reps, cost = Cityblock(), run)
#path = joinpath("data", run * ".dat")
#save_mcsim(path, r)
#println("saved run '$run' under '$path'")

