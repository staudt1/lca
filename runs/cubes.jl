
include("../lca.jl")

settings = [ Cubes(d1, d2) for d1 in 1:10, d2 in [10, 100, 1000] ]
ns = [round(Int, 2^(k/4)) for k in (5*4):(11*4)]
reps = 2000

run = "cube-sqeuclidean"
r = run_mcsim(settings, ns; reps, cost = SqEuclidean(), run)
path = joinpath("data", run * ".dat")
save_mcsim(path, r)
println("saved run '$run' under '$path'")

run = "cube-cityblock"
r = run_mcsim(settings, ns; reps, cost = Cityblock(), run)
path = joinpath("data", run * ".dat")
save_mcsim(path, r)
println("saved run '$run' under '$path'")

