
# Lower Complexity Adaptation - Code Repository

This repository contains the [julia](www.julialang.org) source code used for the numerical examples in [this](https://arxiv.org/abs/2202.10434) publication on *Lower Complexity Adaption* of optimal transport.

The logic and implementation of various settings that we tested can be found in
the file `lca.jl`. The functions in this file expect an OT solver, for example
the one provided by the `PythonOT` package, which calls the
[POT](https://pythonot.github.io/) python package in the background. For the
actual simulations in the paper, we called
[this](https://github.com/nbonneel/network_simplex) C++ based network-flow OT
solver by Nicolas Bonneel, which the `PythonOT` package also relies upon (this
helped us avoide threading issues).

## Usage

Note that the code was only tested on linux, with julia version 1.6. As
a prerequisite, the `Distances` julia package is needed.

If you want to use Bonneel's solver directly, you must copy the files
`full_bipartitegraph.h` and `network_simplex_simple.h` from the [original
repository](https://github.com/nbonneel/network_simplex) to the `emd_bonneel/`
folder of this repository and then build the corresponding shared library
`emd_bonnel.so` (see the `makefile`). Unfortunately, since the code for the
solver has no public license, we cannot include it in this repository.

The code can also be run with other OT solvers. The example below
shows how this works for the `PythonOT` package.

```julia
using PythonOT
include("lca.jl")

# Define a OT problem of type TransportProblem
problem = Cubes(3, 10)

# Get the true (population) OT cost with squared Euclidean ground costs
truth = ot(problem, cost = SqEuclidean())

# Calculate the empirical OT value based on 100 samples
# using the PythonOT.emd2 solver. Repeat this 50 times.
values = eot(problem, 100, cost = SqEuclidean(), reps = 50, solver = emd2)
```
