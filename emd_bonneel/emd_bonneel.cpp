// Minimal wrapper "extern C" function to use the network flow solver by Nicolas
// Bonneel from julia. Inspired by the example main.cpp provided by
//
//   https://github.com/nbonneel/network_simplex
//

#include <iostream>
#include "network_simplex_simple.h"

using namespace lemon;

typedef int64_t arc_id_type; 
typedef double supply_type;
typedef double cost_type;  

extern "C" {

  cost_type emd_bonneel(supply_type *a, supply_type *b, cost_type *c, arc_id_type n1, arc_id_type n2) {

    typedef FullBipartiteDigraph Digraph;
    DIGRAPH_TYPEDEFS(FullBipartiteDigraph);

    std::vector<supply_type> va(n1), vb(n2);

    Digraph di(n1, n2);
    NetworkSimplexSimple<Digraph, supply_type, cost_type, arc_id_type> net(di, true, n1 + n2, n1 * n2);

    arc_id_type idarc = 0;
    for (int i = 0; i < n1; i++) {
      for (int j = 0; j < n2; j++) {
        cost_type d = c[i + n1 * j];
        net.setCost(di.arcFromId(idarc), d);
        idarc++;
      }
    }

    for (int i = 0; i < n1; i++) {
      va[di.nodeFromId(i)] = a[i];
    }

    for (int j = 0; j < n2; j++) {
      vb[di.nodeFromId(j)] = -b[j];
    }

    net.supplyMap(&va[0], n1, &vb[0], n2);

    int ret = net.run();
    if (ret == (int) net.UNBOUNDED) {
      std::cerr << "emd_bonneel: problem is unbounded" << std::endl;
    }
    else if (ret == (int) net.INFEASIBLE) {
      std::cerr << "emd_bonneel: problem is infeasible" << std::endl;
    }

    double emd = net.totalCost();
    return emd;
  }

}
