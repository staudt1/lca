
const lib = "emd_bonneel/emd_bonneel.so"

function emd_bonneel(a :: Vector{Float64}, b :: Vector{Float64}, c :: Matrix{Float64})
  ccall( (:emd_bonneel, lib)
       , Float64
       , (Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Int64, Int64)
       , a, b, c, length(a), length(b) )
end

