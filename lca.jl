
# Julia standard library packages
using Random
using Statistics
using LinearAlgebra
import Serialization

# External packages
using Distances

# Add simple Hoelder distances, which are not part of the Distances package
struct Hoelder <: Distances.UnionMetric
  p :: Float64
end

@inline Distances.eval_op(dist :: Hoelder, ai, bi) = abs(ai - bi)^dist.p
@inline (dist::Hoelder)(a, b) = Distances._evaluate(dist, a, b)


# We use the C++ code of Nicolas Bonneel directly, instead of importing it via
# the python POT package, since multithreading didn't work via PyCall.
# To make the solver work, just type `make` in the subfolder `emd_bonneel`
# of this repository (at least, if you are on linux)
include("emd_bonneel.jl")

# If you want to run this code and are not a linux user, you can uncomment the
# line below, which loads the python OT (POT) package (which has to be installed in
# the python environment used by PyCall, see the documentation of PyCall). Then,
# just use PythonOT.emd2 as solver in the routines below.
# Note, however, that this does *not* seem to work when starting julia with
# multiple threads.
#using PythonOT

# Since we use a large number of threads to compute the empirical OT cost
# for different parameters, allowing additional multi-threading of BLAS seems to
# reduce the performance.
BLAS.set_num_threads(1)

"""
Type of an optimal transport setting
"""
abstract type TransportProblem end

# Below, an interface for a TransportProblem is defined. These functions
# should be implemented for each subtype of TransportProblem.

"""
    uniform_mass(tp)

Whether the transport problem `tp` assumes empirical measures, meaning that each
point carries the same mass.
"""
uniform_mass(:: TransportProblem) = true

"""
    problem_name(tp)

Get a string that represents the optimal transport problem.
"""
problem_name(tp :: TransportProblem) = tp |> Symbol |> String

"""
  ot(tp; cost)

Get the true optimal transport value (or a very precise approximation) for the
given transport problem `tp` with cost `cost`. This is used for centering and
calculating the bias.
"""
ot(tp :: TransportProblem; cost) = ot(tp, cost)
ot(tp :: TransportProblem, cost) = error("implementation missing")

"""
  gen_data(tp, n)

Generate data points for the transport problem `tp`. The integer `n` is
typically the number of data points to be generated for both X and Y, but
individual problems may deviate and just use `n` as a general size indicator.
"""
gen_data(:: TransportProblem, n) = error("implementation missing")

"""
  eot(tp, n; reps = 100, cost = SqEuclidean(), solver = emd_bonneel)

Calculate `reps` empirical estimates of `ot(tp; cost)` with size indicator
`n`. For the solution of the discrete optimal transport problem, `solver` is
used.

The calculations are by default threaded. For non-thread safe solvers (e.g.,
`PythonOT`'s `emd2`), this means that julia must not be started with more than
one thread.
"""
function eot( tp :: TransportProblem
            , n :: Int
            ; reps = 100
            , cost = SqEuclidean()
            , solver = emd_bonneel )

  # For each repetition, generate x and y data, calculate the cost matrix, and
  # apply the solver. A vector with the resulting n values is returned
  res = zeros(Float64, reps)

  # Note: using threading here breaks PythonOT.emd2 when several threads are
  # used, so we wrapped the original c++ implementation by Bonneel in
  # 'emd_bonneel.jl'
  Threads.@threads for i in 1:reps
    
    # distingush between problems where each mass is a uniform point mass and
    # these where we have different weights on points
    if uniform_mass(tp)
      xs, ys = gen_data(tp, n)
      a = ones(size(xs, 2)) / size(xs, 2)
      b = ones(size(ys, 2)) / size(ys, 2)
    else
      xs, ys, a, b = gen_data(tp, n)
    end

    # Calculate the cost matrix
    c = pairwise(cost, xs, ys, dims = 2)

    # Colve and save
    res[i] = solver(a, b, c)

    # Regularly garbage-collect if the problem is large
    # We should not have to do this, but we went swapping in some tests without
    # it...
    if length(c) > 1e6 GC.gc() end
  end
  
  # Return vector with empirical values
  res
end

"""
    run_mcsim(problems, ns; reps, cost, solver, run)

Run several Monte-Carlo simulations of the form `eot(tp, n; reps, cost,
solver)` for `(tp, n)` in `problems × ns`. The argument `run` is a string that
is meant as Meta-Data to keep track of individual calls to `run_mcsim`.
"""
function run_mcsim( problems
                  , ns
                  ; reps = 100
                  , cost = SqEuclidean()
                  , solver = emd_bonneel
                  , run = randstring(5))

  println("starting run '$run'")

  # Some meta-data to interpret the datasets
  cost_string = typeof(cost) |> Symbol |> String
  solver_string = solver |> Symbol |> String

  # Collect all results in an array of named tuples
  map(problems) do tp
    
    # Even more meta-data
    problem_string = problem_name(tp)
    println("calculating / approximating true OT value")
    truth = ot(tp; cost)

    # Some progress logging
    println("transport problem $tp:")
    data = mapreduce(hcat, ns) do n
      dt = @elapsed r = eot(tp, n; reps, cost, solver)
      println("  $n ($dt seconds)")
      [r, dt]
    end
    ( run = run, tp = tp, ns = ns
    , reps = reps, data = data[1,:], runtime = data[2,:], truth = truth
    , problem = problem_string, cost = cost_string, solver = solver_string)
  end
end


"""
    save_mcsim(name, data)

Save the results of a call to `run_mcsim`.
"""
function save_mcsim(fname, results)
  Serialization.serialize(fname, results)
end

"""
    load_mcsim(name)

Load the results of a call to `run_mcsim`.
"""
function load_mcsim(fname)
  Serialization.deserialize(fname)
end


# What follows are different implementations of Optimal transport problems

"""
Simple OT setting where a `d1` dimensional unit-cube is situated within a `d2
> d1` dimensional unit-cube. Uniform mass distribution is assumed.
"""
struct Cubes <: TransportProblem
  d1 :: Int
  d2 :: Int

  function Cubes(d1, d2)
    @assert d1 <= d2 "first dimension must not be larger than second one"
    new(d1, d2)
  end
end

ot(tp :: Cubes, cost :: SqEuclidean) = (tp.d2 - tp.d1) / 3
ot(tp :: Cubes, cost :: Cityblock) = (tp.d2 - tp.d1) / 2
ot(tp :: Cubes, cost :: Hoelder) = (tp.d2 - tp.d1) / (cost.p + 1)

function gen_data(tp :: Cubes, n)
  xs = [rand(tp.d1, n); zeros(tp.d2-tp.d1, n)] 
  ys = rand(tp.d2, n)
  xs, ys
end

"""
OT setting where a `d1` dimensional unit-sphere is situated within a `d2 > d1`
dimensional unit-sphere. Uniform mass distribution is assumed.
"""
struct Spheres <: TransportProblem
  d1 :: Int
  d2 :: Int

  function Spheres(d1, d2)
    @assert d1 <= d2 "first dimension must not be larger than second one"
    new(d1, d2)
  end
end

# In this case, we did not derive an analytical formula for the population OT
# cost. However, it is easy to find the OT map, which is just the projection
# on the smaller sphere. Therefore, we can precisely approximate the true OT value.
function ot(tp :: Spheres, cost)

  # We split the calculation in n blocks where k costs are calculated
  # concurrently. So the total number of projections calculated is k * n
  n = 100000
  k = 1000
  s = zeros(n)

  Threads.@threads for i in 1:n

    # Get a point on the d2-sphere via normalizing a normally drawn point
    y = randn(tp.d2 + 1, k)
    y = y ./ sqrt.(sum(abs2, y, dims = 1))

    # Get the projection to the d1-sphere
    x = copy(y)
    x[tp.d1+2:end, :] .= 0
    x = x ./ sqrt.(sum(abs2, x, dims = 1))

    # Their cost
    s[i] = mean(colwise(cost, x, y))
  end
  sum(s) / n
end

function gen_data(tp :: Spheres, n)
  xs = [randn(tp.d1+1, n); zeros(tp.d2-tp.d1, n)] 
  ys = randn(tp.d2+1, n)
  xs = xs ./ sqrt.(sum(abs2, xs, dims = 1))
  ys = ys ./ sqrt.(sum(abs2, ys, dims = 1))
  xs, ys
end

"""
OT setting where a d-dimensional sphere with radius 1 sits inside of
a d-dimensional sphere with radius 2.
"""
struct Shells <: TransportProblem
  d :: Int
end

ot(tp :: Shells, cost :: SqEuclidean) = 1.

function gen_data(tp :: Shells, n)
  xs = randn(tp.d+1, n)
  ys = randn(tp.d+1, n)
  xs = xs ./ sqrt.(sum(abs2, xs, dims = 1))      # data on sphere with radius 1
  ys = 2 * ys ./ sqrt.(sum(abs2, ys, dims = 1))  # data on sphere with radius 2
  xs, ys
end

"""
Degenerate OT setting in which `μ = μ', ν = p * μ' + (1-p) * ν'`, meaning
that part of the mass lies on the measure `μ`.

Note: This should only be used for Problems in which points from `μ` and `ν` are
drawn independently!
"""
struct Degenerate{T} <: TransportProblem
  tp :: T
  p :: Float64
end

ot(dtp :: Degenerate, cost) = (1 - dtp.p) * ot(dtp.tp, cost)

function gen_data(dtp :: Degenerate, n)
  nrep = sum(rand(n) .<= dtp.p)
  xs, ys = gen_data(dtp.tp, n)
  xsr, _ = gen_data(dtp.tp, nrep)
  ys[:, 1:nrep] .= xsr
  xs, ys
end


"""
Semidiscrete OT problem with voronoi-derived transport maps. For `μ`, we draw some points
in the `d2` dimensional unit cube and assign them the mass proportional to their
voroni-volume. The measure `ν` is uniformly distributed on the cube. Thus, the optimal
transport map from `ν` to `μ` is simply the projection to the closest point in `μ`.

Note: For semidiscrete problems, `ot(...)` must be called before `eot(...)`,
since the former assigns the "Voronoi"-probabilities!
"""
struct Semidiscrete <: TransportProblem
  m :: Int
  d2 :: Int
  pos :: Matrix{Float64}
  prob :: Vector{Float64}

  function Semidiscrete(m :: Int, d2 :: Int)
    # Get random positions that are the same for each pair (m, d2) call
    seed = UInt32[m, d2]
    pos = rand(MersenneTwister(seed), d2, m)
    prob = zeros(Float64, m) # Gets assigned when ot(...) is called
    new(m, d2, pos, prob)
  end
end

# Have to implement the next two functions since semidiscrete problems are
# special compared to the previous problems
problem_name(tp :: Semidiscrete) = "Semidiscrete($(tp.m), $(tp.d2))"
uniform_mass(:: Semidiscrete) = false

# We brute-force an approximation to the voronoi volumes AND the true OT value
# by calculating a lot of projections from randomly drawn points in [0, 1]^d to
# the m selected positions
function ot(tp :: Semidiscrete, cost)
  n = 100000
  k = 1000
  s = zeros(n)
  p = zeros(Int, n, tp.m)
  Threads.@threads for i in 1:n
    y = rand(tp.d2, k)
    r = pairwise(cost, tp.pos, y)
    costs, idxs = findmin(r, dims = 1)
    s[i] = mean(costs)
    for idx in idxs
      p[i, Tuple(idx)[1]] += 1
    end
  end
  p = reshape(sum(p, dims = 1), :)
  tp.prob .= p / (n*k)
  sum(s) / n
end

function gen_data(tp :: Semidiscrete, n)
  xs = tp.pos
  ys = rand(tp.d2, n)
  a = tp.prob
  b = ones(n) / n
  xs, ys, a, b
end

